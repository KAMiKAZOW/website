+++
title = "Welcome"
sort_by = "none"
template = "index/home.html"
insert_anchor_links = "left"
+++

Asus-Linux.org is an independent community effort that works to improve Linux support for Asus notebooks.

Our work consists of Linux Kernel work we send upstream, `dkms` modules for older kernels, and [`asusctl`](https://gitlab.com/asus-linux/asusctl) to give users and third party applications the ability to control Asus hardware functions.

Currently we support:

* Control graphics modes without reboots for switching between iGPU, dGPU, on-demand, and vfio (for VM pass-through)
* Set battery charge limit
* Fan curve control
* Control LEDs, including per-key backlight LEDs and AniMe Matrix

Join the [Discord community](https://discord.gg/scssgeHH) and fork us on [GitLab.com](https://gitlab.com/asus-linux).
